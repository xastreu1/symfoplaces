<?php

namespace App\Repository;

use App\Entity\Place;
use App\Service\PaginatorService;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Place|null find($id, $lockMode = null, $lockVersion = null)
 * @method Place|null findOneBy(array $criteria, array $orderBy = null)
 * @method Place[]    findAll()
 * @method Place[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PlaceRepository extends ServiceEntityRepository
{
    protected PaginatorService $paginatorService;

    public function __construct(ManagerRegistry $registry, PaginatorService $paginatorService)
    {
        $this->paginatorService = $paginatorService;
        parent::__construct($registry, Place::class);
    }

    public function getPagination($link = 'place_index')
    {
        return [
            'page' => $this->paginatorService->getPage(),
            'total' => $this->paginatorService->getTotal(),
            'total_pages' => $this->paginatorService->getTotalPages(),
            'limit' => $this->paginatorService->getLimit(),
            'link' => $link
        ];
    }

    public function findAllPaginated($page)
    {
        $sql = $this->createQueryBuilder('u')->getQuery();
        return $this->paginatorService->paginate($sql, $page);
    }

}
