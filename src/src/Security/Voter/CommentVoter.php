<?php

namespace App\Security\Voter;

use App\Entity\Comment;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Security;
use App\Entity\User;

class CommentVoter extends Voter
{
    public function __construct(
        protected Security $security
    ){}

    protected function supports(string $attribute, $subject): bool
    {
        return in_array($attribute, ['edit', 'create', 'delete'])
            && $subject instanceof Comment;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();

        // if the user is anonymous, do not grant access
        if (!$user instanceof UserInterface) {
            return false;
        }

        $method = 'can'.ucfirst($attribute);

        return $this->$method($subject, $user);
    }

    private function canEdit(Comment $comment, User $user): bool
    {
        return $user === $comment->getCreatedBy()
            || $user === $comment->getPlace()->getCreatedBy()
            || $this->security->isGranted('ROLE_EDITOR');
    }

    private function canCreate(Comment $comment, User $user): bool
    {
        return true;
    }

    private function canDelete(Comment $comment, User $user): bool
    {
        return $this->canEdit($comment, $user);
    }
}
