<?php

namespace App\Security\Voter;

use App\Entity\Photo;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Security;
use App\Entity\User;

class PhotoVoter extends Voter
{
    public function __construct(
        protected Security $security
    ){}

    protected function supports(string $attribute, $subject): bool
    {
        return in_array($attribute, ['edit', 'create', 'delete'])
            && $subject instanceof Photo;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();

        // if the user is anonymous, do not grant access
        if (!$user instanceof UserInterface) {
            return false;
        }

        $method = 'can'.ucfirst($attribute);

        return $this->$method($subject, $user);
    }

    private function canEdit(Photo $photo, User $user): bool
    {
        return $user === $photo->getUpdatedBy()
            || $user === $photo->getPlace()->getCreatedBy()
            || $this->security->isGranted('ROLE_EDITOR');
    }

    private function canCreate(Photo $photo, User $user): bool
    {
        return true;
    }

    private function canDelete(Photo $photo, User $user): bool
    {
        return $this->canEdit($photo, $user);
    }
}
