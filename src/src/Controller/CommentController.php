<?php

namespace App\Controller;

use App\Entity\Comment;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;

#[Route('/comments')]
class CommentController extends AbstractController
{

    #[Route('/{id}', name: 'comment_delete', methods: ['POST'])]
    public function delete(
        Request $request,
        Comment $comment,
        EntityManagerInterface $entityManager,
        LoggerInterface $appInfoLogger
    ): Response
    {
        if ($this->isCsrfTokenValid('delete'.$comment->getId(), $request->request->get('_token'))) {
            $appInfoLogger->info($this->getUser()->getEmail() . " deleted comment.");
            $entityManager->remove($comment);
            $entityManager->flush();
        }

        return $this->redirectToRoute('place_index', [], Response::HTTP_SEE_OTHER);
    }
}
