<?php

namespace App\Controller;

use App\Entity\Photo;
use App\Service\FileUploaderService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;

#[Route('/photos')]
class PhotoController extends AbstractController
{

    #[Route('/{id}', name: 'photo_delete', methods: ['POST'])]
    public function delete(
        Request $request,
        Photo $photo,
        EntityManagerInterface $entityManager,
        FileUploaderService $fileUploaderService
    ): Response
    {
        if ($this->isCsrfTokenValid('delete'.$photo->getId(), $request->request->get('_token'))) {
            $fileUploaderService->remove($photo->getImage());
            $entityManager->remove($photo);
            $entityManager->flush();
        }

        return $this->redirectToRoute('place_index', [], Response::HTTP_SEE_OTHER);
    }
}
