<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\Photo;
use App\Entity\Place;
use App\Form\CommentType;
use App\Form\PhotoType;
use App\Form\PlaceType;
use App\Repository\PlaceRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\FileUploaderService;
use Psr\Log\LoggerInterface;
use App\Service\SimpleSearchService;
use App\Form\SearchPlaceType;

#[Route('/places')]
class PlaceController extends AbstractController
{
    #[Route('/', name: 'place_index', methods: ['GET'])]
    public function index(Request $request, PlaceRepository $placeRepository): Response
    {
        $page = $request->query->has('page') ? $request->query->get('page') : 1;
        return $this->render('place/index.html.twig', [
            'places' => $placeRepository->findAllPaginated($page),
            'pagination' => $placeRepository->getPagination(),
        ]);
    }

    #[Route('/new', name: 'place_new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager, LoggerInterface $appInfoLogger): Response
    {
        $place = new Place();
        $form = $this->createForm(PlaceType::class, $place);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $appInfoLogger->info($this->getUser()->getEmail() . " added place " .$place->getName());
            $place->setCreatedBy($this->getUser());
            $entityManager->persist($place);
            $entityManager->flush();

            return $this->redirectToRoute('place_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('place/new.html.twig', [
            'place' => $place,
            'form' => $form,
        ]);
    }

    #[Route('/search', name: 'place_search', methods: ['GET', 'POST'])]
    public function searchFilms(Request $request, PlaceRepository $placeRepository, SimpleSearchService $simpleSearchService)
    {
        $form = $this->createForm(SearchPlaceType::class, null, [
            'field_choices' => [
                'Name' => 'name',
                'Type' => 'type',
            ],
            'order_choices' => [
                'Name' => 'name',
                'Type' => 'type'
            ],
        ]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $places = $simpleSearchService->search(Place::class, $data);
        } else {
            $places = $placeRepository->findAll();
        }

        return $this->render('place/search.html.twig', [
            'places' => $places,
            'search_form' => $form->createView(),
        ]);
    }

    #[Route('/{id}', name: 'place_show', methods: ['GET'])]
    public function show(Place $place): Response
    {
        $commentForm = $this->createForm(CommentType::class, new Comment(), [
            'action' => $this->generateUrl('place_add_comment', [
                'id' => $place->getId(),
            ]),
        ]);

        return $this->render('place/show.html.twig', [
            'place' => $place,
            'commentForm' => $commentForm->createView(),
        ]);
    }

    #[Route('/{id}/edit', name: 'place_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Place $place, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(PlaceType::class, $place);
        $form->handleRequest($request);

        $photoForm = $this->createForm(PhotoType::class, new Photo(), [
            'action' => $this->generateUrl('place_add_photo', [
                'id' => $place->getId(),
            ]),
        ]);

        $commentForm = $this->createForm(CommentType::class, new Comment(), [
            'action' => $this->generateUrl('place_add_comment', [
                'id' => $place->getId(),
            ]),
        ]);


        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('place_show', [
                'id' => $place->getId(),
            ], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('place/edit.html.twig', [
            'place' => $place,
            'form' => $form,
            'photoForm' => $photoForm,
            'commentForm' => $commentForm,
        ]);
    }

    #[Route('/{id}/add/photo', name: 'place_add_photo', methods: ['POST'])]
    public function addPhoto(
        Request $request,
        Place $place,
        EntityManagerInterface $entityManager,
        FileUploaderService $fileUploaderService,
        LoggerInterface $appInfoLogger
    ): Response
    {
        $photo = new Photo();
        $form = $this->createForm(PhotoType::class, $photo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $file = $form->get('file')->getData();
            if ($file) {
                $photo->setImage($fileUploaderService->upload($file,true));
            }
            $appInfoLogger->info($this->getUser()->getEmail() . " added photo " .$photo->getTitle());
            $photo->setUpdatedBy($this->getUser());
            $photo->setPlace($place);
            $entityManager->persist($photo);
            $entityManager->flush();

            return $this->redirectToRoute('place_show', [
                'id' => $place->getId(),
            ], Response::HTTP_SEE_OTHER);
        }

        return $this->createNotFoundException();
    }

    #[Route('/{id}/add/comment', name: 'place_add_comment', methods: ['POST'])]
    public function addComment(Request $request, Place $place, EntityManagerInterface $entityManager, LoggerInterface $appInfoLogger): Response
    {
        $comment = new Comment();
        $form = $this->createForm(CommentType::class, $comment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $comment->setCreatedBy($this->getUser());
            $appInfoLogger->info($this->getUser()->getEmail() . " added comment " .$comment->getMessage());
            $comment->setPlace($place);
            $entityManager->persist($comment);
            $entityManager->flush();

            return $this->redirectToRoute('place_show', [
                'id' => $place->getId(),
            ], Response::HTTP_SEE_OTHER);
        }

        return $this->createNotFoundException();
    }

    #[Route('/{id}', name: 'place_delete', methods: ['POST'])]
    public function delete(Request $request, Place $place, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$place->getId(), $request->request->get('_token'))) {
            $entityManager->remove($place);
            $entityManager->flush();
        }

        return $this->redirectToRoute('place_index', [], Response::HTTP_SEE_OTHER);
    }

}
