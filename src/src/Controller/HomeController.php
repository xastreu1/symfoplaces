<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use App\Form\ContactFormType;

class HomeController extends AbstractController
{
    #[Route('/', name: 'home')]
    public function index(): Response
    {
        return $this->redirectToRoute('place_index');
    }

    #[Route('/contact', name: 'contact_form', methods: ['GET','POST'])]
    public function contact(Request $request, MailerInterface $mailer)
    {
        $form = $this->createForm(ContactFormType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $email = (new TemplatedEmail())
                ->from('bcnspeeddating@gmail.com')
                ->to('arodriguez@leadwizzer.com')
                ->subject($data['subject'])
                ->htmlTemplate('home/email-template.html.twig')
                ->context([
                    'message' => $data['message'],
                ]);
            $mailer->send($email);
            $this->addFlash('success', "The email was sent correctly");
            return $this->redirectToRoute('home');
        }

        return $this->renderForm('home/contact.html.twig',[
            'form' => $form,
        ]);

    }
}
